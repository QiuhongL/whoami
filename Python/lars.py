# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 18:28:23 2018

@author: Lai
"""
import numpy as np
from scipy import linalg

class lars():#lars迭代器版本
    
    def __init__(self,x,y,lambda1=None):
        self.lambda1 = lambda1#array 要求从大到小排序
        self.n,self.p = x.shape
        xm = x.mean(axis=0)
        ym = y.mean()
        self.xtx = np.dot(x.T,x)-self.n*np.outer(xm,xm)
        self.xty = np.dot(x.T,y)-self.n*xm*ym
        self.w = np.sqrt(np.diag(self.xtx))
        self.w1 = 1/self.w
        j = np.argmax(self.xty/self.w)
        self.VA = [j]#list
        self.VAc = np.delete(range(self.p),j)#array
        self.L = self.w[j].reshape((1,1))
        self.b =np.zeros(self.p)#array
        self.lamb = self.w1[j]*abs(self.xty[j])#array
        self.relamb = [self.lamb]#list
        if any(self.lambda1):self.reb = np.zeros((len(self.lambda1),self.p))
        else:self.reb = self.b.copy()#array
        print('Your iterator has been initialized *V*')
        
    @staticmethod
    def givens(mx,lmx):
        mc = mx[0]/lmx
        ms = mx[1]/lmx
        return np.array([[mc,-ms],[ms,mc]],dtype='float')
    
    def mgivens(self,L,k):
        p,_ = L.shape
        if k>p:return 'Wrong input of k!'
        Lk = np.delete(L,k-1,axis=0) 
        for i in range(int(k-1),int(p-1)):
            mx = Lk[i,i:(i+2)]
            lmx = np.sqrt(sum(mx*mx))
            Lk[i,i:(i+2)] = lmx,0
            if i<p-2:Lk[(i+1):(p-1),i:(i+2)]=np.dot(Lk[(i+1):(p-1),i:(i+2)],self.givens(mx,lmx)) 
        return np.delete(Lk,p-1,axis=1)
        
    @staticmethod
    def forupdate(L,xxk,xkxk,n_active):
        lk = linalg.solve_triangular(L,xxk,lower=True)
        lkk = np.sqrt(xkxk-sum(lk*lk))
        return np.r_[np.c_[L,np.zeros(L.shape[0])],np.c_[np.array(lk).reshape((1,-1)),lkk]]
    
    def __next__(self):
        n_active = len(self.VA)
        CC = self.w1*(self.xty-np.dot(self.xtx,self.b))
        SCC = np.sign(CC)
        SCCA = SCC[self.VA]
        td = linalg.solve_triangular(self.L,self.w[self.VA]*SCCA,lower=True)
        d = linalg.solve_triangular(self.L.T,td,lower=False)
        a = self.w1[self.VAc]* np.dot(self.xtx[:,self.VA][self.VAc,:].reshape((-1,n_active)),d)
        gam = np.zeros(self.p)
        ww = -self.b[self.VA]/d
        gam[self.VA] = np.where(ww*(self.lamb-ww)>0,ww,self.lamb) 
        eps = 10e-5
        mm = max(gam[self.VA])+eps
        gam[self.VAc] = np.where(a*self.lamb<=CC[self.VAc],(self.lamb-CC[self.VAc])/(1-a),(self.lamb+CC[self.VAc])/(1+a)) 
        gam[gam<=0] = mm
        j = np.argmin(gam)
        gammin = gam[j]
        if any(self.lambda1):#array
            ind = np.where((self.lamb-self.lambda1)*(self.lambda1-self.lamb-gammin+eps)>0)[0]
            num = len(ind)
            ind_x = (np.ones((n_active,num),dtype='int64')*ind).reshape((1,-1),order='F').tolist()#list
            ind_y1 = self.VA*num
            ind_y2 = self.VAc*num
            self.reb[ind_x,ind_y1] = (np.ones((len(ind),n_active))*self.b[self.VA]+np.dot((self.lamb-self.lambda1[ind]).reshape((-1,1)),d.reshape((1,-1)))).reshape((1,-1))
            self.reb[ind_x,ind_y2] = self.b[self.VAc].tolist()*num
        self.b[self.VA]+=gammin*d
        self.lamb -=gammin
        self.relamb=np.r_[self.relamb,self.lamb]
        if not any(self.lambda1):self.reb=np.r_[self.reb.reshape((-1,self.p)),self.b.reshape((1,-1))]  
        if self.lamb==0:
            print('current lambda:')
            print(0)
            print('Iteration finished!')
            raise StopIteration#停止迭代条件
        jj = np.where(np.array(self.VA)==j)
        if not len(jj[0]): 
            XTXAJ = self.xtx[self.VA,j]
            XTXJJ = self.xtx[j,j]
            self.L = self.forupdate(self.L,XTXAJ,XTXJJ,n_active)
            self.VA.append(j)
            self.VAc = np.delete(range(self.p),self.VA)
        else:
            self.L = self.mgivens(self.L,jj[0])
            self.VA = np.delete(self.VA,jj[0]).tolist()
            self.VAc = np.delete(range(self.p),self.VA)
        print('current lambda:')    
        return self.lamb
    
    def __iter__(self):
        return self
############################example
x= np.loadtxt(r'C:\Users\Administrator\Desktop\prostate_x.csv', skiprows=1)
y= np.loadtxt(r'C:\Users\Administrator\Desktop\prostate_y.csv', skiprows=1)
mylars1 = lars(x,y)#迭代器初始化
for i in mylars1:print(i)#运行
mylars1.reb
mylars1.relamb

mylars2 = lars(x,y,np.array([6,5,4,3,2,1,0.1]))#迭代器初始化
for i in mylars2:print(i)#运行
mylars2.reb
mylars2.relamb


#####################finish!!!!    
    
class A():
    @staticmethod
    def m(L):
        p,_ = L.shape
        Lk = np.delete(L,k,axis=0) 
        return Lk
    