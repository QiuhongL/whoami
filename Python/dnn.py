# -*- coding: utf-8 -*-
"""
Created on Sun Oct  8 12:14:37 2017

@author: Lai
"""

import numpy as np
import random
###读入数据
xdigit_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\azip.dat','r')
ydigit_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\dzip.dat','r')
test_y_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\dtest.dat','r')
test_x_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\testzip.dat','r')
xdigit = xdigit_f.readlines()
ydigit = ydigit_f.readlines()
test_y = test_y_f.readlines()
test_x = test_x_f.readlines()
xdigit_f.close()
ydigit_f.close()
test_x_f.close()
test_y_f.close()

###将数据存为list
train_data_x = [] 
x =  np.mat(np.zeros((256,1707))) 
i = 0
for line in xdigit:
    li =line.strip(' ').split('  ')
    x[i,:] = li
    i+=1
for i in range(1707):    
    train_data_x.append(x[:,i])

train_data_y = []
yhat = np.mat(np.zeros((10,1707)))
i = 0
yli = ydigit[0].strip(' ').split('  ')
for yr in yli:
    yhat[int(float(yr)),i] = 1
    i += 1
for i in range(1707):    
    train_data_y.append(yhat[:,i])

train_data = zip(train_data_x,train_data_y)   

    
test_data_x = []
x_test =  np.mat(np.zeros((256,2007))) 
i = 0
for line in test_x:
    li =line.strip(' ').split('  ')
    x_test[i,:] = li
    i+=1
for i in range(2007):    
    test_data_x.append(x_test[:,i])
    
test_data_y = [int(float(num)) for num in test_y[0].strip(' ').split('  ')]  
    
test_data = zip(test_data_x,test_data_y)    

###my neural network
def sigmoid(z):
    return 1.0/(1.0 + np.exp(-z))


def dsigmoid(z):
    return np.multiply(sigmoid(z),1-sigmoid(z))

def dcost(y,yhat):
    return y-yhat

def evaluation(test_data_x,test_data_y,weights,bias):
    correct = 0
    for t_x,t_y in zip(test_data_x,test_data_y):
        t_a = t_x
        for w,b in zip(weights,bias):
            t_a = sigmoid(np.dot(w,t_a) + b)     
        if np.argmax(t_a) == t_y:
            correct += 1
    return correct

def dnn(train_data_x,train_data_y,test_data_x,test_data_y,layers_sizes,
        weights,bias,epoch,eta,lamb,batch_size):
    for k in range(epoch):#对于每一个周期打乱样本次序
        train_data_x_copy = []
        train_data_y_copy = []
        index =[i for i in range(len(train_data_x))]
        random.shuffle(index)
        for i in index:
            train_data_x_copy.append(train_data_x[i])
            train_data_y_copy.append(train_data_y[i])
        mini_batch_x = [train_data_x_copy[k:k+batch_size] for k 
                        in range(0,len(train_data_x),batch_size)]#抽取mini-batch
        mini_batch_y = [train_data_y_copy[k:k+batch_size] for k 
                        in range(0,len(train_data_x),batch_size)]
        for mini_x,mini_yhat in zip(mini_batch_x,mini_batch_y):
            sum_gs_w = [np.zeros((n,m)) for n,m in zip(layers_sizes[1:],
                        layers_sizes[:-1])]#存储对于一个mini-batch的梯度加和
            sum_gs_b = [np.zeros((n,1)) for n in layers_sizes[1:]]
            for x,yhat in zip(mini_x,mini_yhat):#对于一个mini-batch中的每个数据
                zs = []#存储每一个layer的输入
                ass = [x]#存储每一个layer的输出
                a = x
                for w,b in zip(weights,bias):# forward pass
                    z = np.dot(w,a) + b
                    zs.append(z)
                    a = sigmoid(z)
                    ass.append(a)
             
                delta = np.multiply(dsigmoid(zs[-1]),dcost(ass[-1],yhat))#最后一层的误差值
                gs_w = [np.zeros((n,m)) for n,m in zip(layers_sizes[1:],
                        layers_sizes[:-1])]#存储一个数据的梯度
                gs_b = [np.zeros((n,1)) for n in layers_sizes[1:]]
                gs_w[-1] = np.dot(delta,ass[-2].T)#最后一层的梯度
                gs_b[-1] = delta
                for l in range(2,len(weights)+1):#计算各层的误差以及梯度
                    delta = np.multiply(dsigmoid(zs[-l]),
                                        np.dot(weights[-l+1].T,delta))
                    g_w = np.dot(delta,ass[-l-1].T)
                    g_b = delta
                    gs_w[-l] = g_w
                    gs_b[-l] = g_b
                sum_gs_w = [sum_w + s_w for sum_w,s_w in zip(sum_gs_w,gs_w)]
                sum_gs_b = [sum_b + s_b for sum_b,s_b in zip(sum_gs_b,gs_b)]
            weights = [(1-eta*lamb)*old_w - (eta/len(mini_x))*s_w for old_w,s_w 
                       in zip(weights,sum_gs_w)]#gradient descent
            bias = [old_b - (eta/len(mini_x))*s_b for old_b,s_b 
                    in zip(bias,sum_gs_b)]
        print("epoch {0}:{1}/{2}".format(k,evaluation(test_data_x,test_data_y
              ,weights,bias),len(test_data_x))) 
        
###use dnn        
layers_sizes =[256,30,10]
weights = [np.random.randn(n,m) for n,m in zip(layers_sizes[1:],
           layers_sizes[:-1])]
bias = [np.random.randn(n,1) for n in layers_sizes[1:]]
epoch = 50
batch_size = 10
eta = 3#learning rate
lamb = 0.0001#正则项系数
dnn(train_data_x,train_data_y,test_data_x,test_data_y,layers_sizes,
        weights,bias,epoch,eta,lamb,batch_size)

  
       
    
            
            
            
    

    
    



