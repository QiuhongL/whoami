# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 13:45:23 2018

@author: Lai
"""
import pandas as pd
import jieba
import collections 
import re
import os
import os.path
import pickle as pkl
import matplotlib.pyplot as plt
#from statsmodels.tsa.stattools import adfuller
#from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
#from statsmodels.tsa.arima_model import ARIMA
import math
from sklearn.decomposition import TruncatedSVD
import numpy as np

def sen2vec_SIF(w_embedding,sentence_list,word_list,a=1e-3):
    def zero():
        return 0
    w_counter = collections.defaultdict(zero)
    for sent in sentence_list:
        s = sent.split()
        for w in s:
            w_counter['w']+=1
    n = sentence_list.__len__()
    SIF_emb = np.zeros((n, w_embedding.shape[1]))
    w_n = sum(w_counter.values())
    w_p = {w:a/(a+w_counter[w]/w_n) for w in word_list}
    w_id = {w:word_list.index(w) for w in word_list}
    i = 0
    for sent in sentence_list:
        s = sent.split()
        SIF_emb[i,:] = np.array([w_p.setdefault(i,0) for i in s]).dot(w_embedding[[w_id.setdefault(i,0) for i in s],:])/s.__len__()
        i += 1

    svd = TruncatedSVD(n_components=1, n_iter=7, random_state=0)
    svd.fit(SIF_emb)
    pc = svd.components_
    SIF_emb = SIF_emb - SIF_emb.dot(pc.transpose()) * pc
    return SIF_emb  

def sen2vec_MAX(w_embedding,sentence_list,word_list):
    n = sentence_list.__len__()
    sen_emb = np.zeros((n, w_embedding.shape[1]))
    w_id = {w:word_list.index(w) for w in word_list}
    num = 0
    for sent in sentence_list:
        s = sent.split()
        w_list = list(set(s).intersection(set(word_list)))
        nn = w_list.__len__()
        sen_e = np.zeros((nn, w_embedding.shape[1]))
        for i in range(nn):
           sen_e[i]=w_embedding[w_id[w_list[i]]]
        sen_emb[num] = np.max(sen_e,axis=0) 
        num += 1
        
    return sen_emb     
        
       		
# save data in a dictionary in which each item is a variable wanted to be reused    
#def save_data(save_path,file_name,var_list):
#    data = {}
#    for var in var_list:
#        data[str(var)] = var
#        param_path = os.path.join(self.logdir+'/model_para/params.pkl')
#    if os.path.exists(save_path+'/'+file_name+'.pkl'):
#        os.remove(save_path+'/'+file_name+'.pkl')
#    with open(save_path+'/'+file_name+'.pkl','wb') as f:
#        pkl.dump(data,f)  
        


