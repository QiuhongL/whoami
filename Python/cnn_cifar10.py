# -*- coding: utf-8 -*-
"""
Created on Sat Nov  4 13:08:53 2017

@author: Lai
"""

import numpy as np
from PIL import Image
import random

def load_batch(file):
    ''' load a single batch'''
    import pickle
    with open(file, 'rb') as fo:
        datadict = pickle.load(fo, encoding='latin1')
        x = datadict['data'].astype('float')
        y = datadict['labels']
    return x,y

xs = []
ys = []
for b in range(1,6):
    x, y = load_batch('data_batch_%d'%b)
    xs.append(x)
    ys.append(y)
    xtr = np.concatenate(xs)
    ytr = np.concatenate(ys)
del xs, ys, x, y, b   
  
xte, yte = load_batch('test_batch')
yte = np.array(yte)

my_im = Image.fromarray(xtr[90].reshape(3,32,32).astype(np.uint8).transpose())
my_im.show()

def evaluate_for_this_cnn(xte,yte,filter_w1,filter_w2,filter_b1,filter_b2):#计算正确个数
    xte = xte.reshape(-1,3,32,32)
    y = softmax(conv_layer(max_pooling_layer(conv_layer(
            xte,filter_w1,filter_b1)),filter_w2,filter_b2,pad_choice=0))
    return np.sum(np.argmax(y,1).reshape(xte.shape[0])==yte)
    
def softmax(x):
    x = x-(np.ones((x.shape[1],1))*np.max(x,1)).reshape(x.shape)
    return np.multiply(np.exp(x),(np.ones((x.shape[1],1))*(1/np.sum(np.exp(x),
                                          1))).reshape(x.shape))
        
def conv_layer(x,filter_w,filter_b,s=1,pad_choice = 1,relu_choice = 1):
    n,d,w,h = x.shape#图片数量,特征数,图片宽度,图片高度
    nf = filter_w.shape[0]#滤波器个数,尺寸
    f = filter_w.shape[2]
    pad = int((w*(s-1)-s+f)/2)#补零圈数
    x_pad = x
    if pad_choice:
       x_pad = np.pad(x,((0,0),(0,0),(pad, pad),(pad,pad)),'constant')#补零
    nn,dd,ww,hh = x_pad.shape
    n_w = int(1+(ww-f)/s)
    n_h = int(1+(hh-f)/s)
    v = np.zeros((n,nf,n_w,n_h))#储存输出数据体
    for i in range(n):
        for j in range(nf):
            for  k in range(n_h):
                 for l in range(n_w):
                     v[i,j,k,l] = np.sum(np.multiply(x_pad[i,:,k*s:k*s+f,
                      l*s:l*s+f],filter_w[j]))+filter_b[j]#卷积
    if relu_choice:
        return np.multiply(v,(v>0))#进入relu激活函数
    return v                
                        
        
    
def max_pooling_layer(x):
    n,d,w,h = x.shape
    p = np.zeros((n,d,int(w/2),int(h/2)))
    for i in range(n):
        for j in range(d):
            for k in range(int(h/2)):
                for l in range(int(w/2)):
                    p[i,j,k,l] = np.max(x[i,j,k*2:(k+1)*2,l*2:(l+1)*2])#池化
    return p                 
                    
                    
    
def backward_conv(delta_in,filter_w,conv_in,conv_out,s=1,relu_choice = 1):
    n,nf,w,h = delta_in.shape
    nn,dd,ww,hh = conv_in.shape
    f = filter_w.shape[2]
    if relu_choice:
        delta_in = np.multiply(delta_in,1*(conv_out>0))
    delta_out = np.zeros_like(conv_in)
    pad = int((s*(ww-1)-w+f)/2)
    delta_in_pad = np.pad(delta_in,((0,0),(0,0),(pad,pad),(pad,pad)),
                            'constant')#补零
    sli = np.zeros((ww,hh))
    for i in range(n):#图片数量
        for j in range(dd):#pooling layer深度
            for k in range(nf):#特征个数
                for l in range(hh):
                    for m in range(ww):
                        sli[l,m] = np.sum(np.multiply(delta_in_pad[i,k,
                           l*s:l*s+f,m*s:m*s+f],(filter_w[k,j][::-1].T)[::-1].T))
                delta_out[i,j] += sli
    db = np.sum(delta_in,(2,3))            
    dw = np.zeros((n,nf,dd,f,f))
    conv_in_pad = np.pad(conv_in,((0,0),(0,0),(pad, pad),(pad,pad)),'constant')#补零            
    for i in range(n):
        for j in range(nf):
            for k in range(h):
                for l in range(w):
                    dw[i,j] += delta_in[i,j,k,l] *conv_in_pad[i,:,k*s:k*s+f,
                      l*s:l*s+f]
    ave_dw = np.mean(dw,0)   
    ave_db = np.mean(db,0)             
    return (delta_out,ave_dw,ave_db)                
    
def backward_pool(delta_in,conv_out,filter_w,filter_b):
    n,nf,w,h = delta_in.shape
    delta_out = np.zeros((n,nf,2*w,2*h))
    for i in range(n):
        for j in range(nf):
            for k in range(h):
                for l in range(w):
                    v = conv_out[i,j,k*2:(k+1)*2,l*2:(l+1)*2]
                    m = np.max(v)
                    delta_out[i,j,k*2:(k+1)*2,l*2:(l+1)*2] = delta_in[i,
                               j,k,l]*(v==m)
    return delta_out                
    
def update(filter_w,filter_b,dw,db,lamb,eta):
    w = (1-eta*lamb)*filter_w - eta*dw
    b = filter_b - eta*db
    return (w,b)
        
  
#build a cnn
n = 10#batch size
epoch =  100
lamb = 0.001
eta = 2
filter_w1 = np.random.randn(20,3,3,3)
filter_b1 = np.random.randn(20)
filter_w2 = np.random.randn(10,20,16,16)
filter_b2 = np.random.randn(10)
for k in range(epoch):
    #k=0
    index = [i for i in range(xtr.shape[0])]
    random.shuffle(index)
    for i in range(int(xtr.shape[0]/n)):
        #i=0
        mini_x = xtr[index[i*n:(i+1)*n]].reshape(-1,3,32,32)
        mini_y = ytr[index[i*n:(i+1)*n]]
        a1 = conv_layer(mini_x,filter_w1,filter_b1)#forward pass
        a2 = max_pooling_layer(a1)
        a3 = conv_layer(a2,filter_w2,filter_b2,pad_choice = 0,relu_choice = 0)
        y = softmax(a3)
        # np.sum(softmax(a3),1)
        delta_y = y
        for i in range(n):
            delta_y[i,mini_y[i]] -= 1
        delta3,dw2,db2 = backward_conv(delta_y,filter_w2,a2,a3,relu_choice = 0)
        delta2 = backward_pool(delta3,a1,filter_w1,filter_b1)
        delta1,dw1,db1 = backward_conv(delta2,filter_w1,mini_x,a1)
        filter_w1,filter_b1 = update(filter_w1,filter_b1,dw1,db1,lamb,eta)
        filter_w2,filter_b2 = update(filter_w2,filter_b2,dw2,db2,lamb,eta)
    print("epoch {0}:{1}/{2}".format(k,evaluate_for_this_cnn(xte,yte
          ,filter_w1,filter_w2,filter_b1,filter_b2),xte.shape[0]))
    
    
    

    


   