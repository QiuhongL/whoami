# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 20:31:26 2017

@author: Lai
"""
import numpy as np
import random
###读入数据
xdigit_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\azip.dat','r')
ydigit_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\dzip.dat','r')
test_y_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\dtest.dat','r')
test_x_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\testzip.dat','r')
xdigit = xdigit_f.readlines()
ydigit = ydigit_f.readlines()
test_y = test_y_f.readlines()
test_x = test_x_f.readlines()
xdigit_f.close()
ydigit_f.close()
test_x_f.close()
test_y_f.close()

###将数据存为list
train_data_x = [] 
x =  np.mat(np.zeros((256,1707))) 
i = 0
for line in xdigit:
    li =line.strip(' ').split('  ')
    x[i,:] = li
    i+=1
for i in range(1707):    
    train_data_x.append(x[:,i])

train_data_y = []
yhat = np.mat(np.zeros((10,1707)))
i = 0
yli = ydigit[0].strip(' ').split('  ')
for yr in yli:
    yhat[int(float(yr)),i] = 1
    i += 1
for i in range(1707):    
    train_data_y.append(yhat[:,i])
    
test_data_x = []
x_test =  np.mat(np.zeros((256,2007))) 
i = 0
for line in test_x:
    li =line.strip(' ').split('  ')
    x_test[i,:] = li
    i+=1
for i in range(2007):    
    test_data_x.append(x_test[:,i])
    
test_data_y = [int(float(num)) for num in test_y[0].strip(' ').split('  ')]  
    
    

time_step = 16
w_0 = np.random.randn(10,10)
w_1 = np.random.randn(10,16)
w_h = np.random.randn(10,10)
b_0 = np.random.randn(10,1)
b_1 = np.random.randn(10,1)
b_h = np.random.randn(10,1)
epoch = 200
batch_size = 10
eta = 0.05
lamb = 0.0001
init = np.mat(np.zeros(10)).T

def sigmoid(z):
    return 1.0/(1.0 + np.exp(-z))

def dsigmoid(z):
    return np.multiply(sigmoid(z),1-sigmoid(z))

def dcost(y,yhat):
    return yhat-y

def evaluate(test_data_x,test_data_y,w_0,w_1,w_h,b_0,b_1,b_h):
    correct = 0
    for x,y in zip(test_data_x,test_data_y):
         x_mat = [ x for x in np.mat(x).reshape((time_step,
                     int(len(x)/time_step)))]
         a_1 = init
         for x_m in x_mat:#forward pass
             a_1 = sigmoid(np.dot(w_1,x_m.T)+b_1+np.dot(w_h,a_1)+b_h) 
         yhat = sigmoid(np.dot(w_0,a_1)+b_0)      
         if np.argmax(yhat) == y:
             correct += 1
    return correct


for k in range(epoch):
    train_data_x_copy = []
    train_data_y_copy = []
    index =[i for i in range(len(train_data_x))]
    random.shuffle(index)
    for i in index:
        train_data_x_copy.append(train_data_x[i])
        train_data_y_copy.append(train_data_y[i])
    mini_batch_x = [train_data_x_copy[k:k+batch_size] for k 
                    in range(0,len(train_data_x),batch_size)]
    mini_batch_y = [train_data_y_copy[k:k+batch_size] for k 
                    in range(0,len(train_data_x),batch_size)]
    for mini_x,mini_y in zip(mini_batch_x,mini_batch_y):
        sum_g_w_0 = np.zeros_like(w_0)
        sum_g_w_1 = np.zeros_like(w_1)
        sum_g_w_h = np.zeros_like(w_h)
        sum_g_b_0 = np.zeros_like(b_0)
        sum_g_b_1 = np.zeros_like(b_1)
        sum_g_b_h = np.zeros_like(b_h)
        for x,y in zip(mini_x,mini_y):
            x_mat = [ x.T for x in np.mat(x).reshape((time_step,
                     int(len(x)/time_step)))]
            a_1 =  init
            zs_1 = []
            zs_h = []
            as_1 = [a_1]
            for x_m in x_mat:#forward pass
               z_1 = np.dot(w_1,x_m)+b_1
               z_h = np.dot(w_h,a_1)+b_h
               a_1 = sigmoid(z_1+z_h)
               zs_1.append(z_1)
               zs_h.append(z_h)
               as_1.append(a_1)
            yhat = sigmoid(np.dot(w_0,as_1[-1])+b_0)
            delta_0 = np.multiply(dcost(y,yhat),
                                 dsigmoid(np.dot(w_0,as_1[-1])+b_0))
            delta_1 = np.multiply(np.dot(w_0.T,delta_0),dsigmoid(zs_1[-1]))
            delta_h = np.multiply(np.dot(w_0.T,delta_0),dsigmoid(zs_h[-1]))
            deltas_1 = [delta_1]
            deltas_h = [delta_h]
            zs_1_0 = zs_1[:-1]
            zs_1_0.reverse()
            zs_h_0 = zs_h[:-1]
            zs_h_0.reverse()
            for z_1,z_h in zip(zs_1_0,zs_h_0):#backward pass
                delta_1 = np.multiply(np.dot(w_h.T,delta_h),dsigmoid(z_1))
                delta_h = np.multiply(np.dot(w_h.T,delta_h),dsigmoid(z_h))
                deltas_1.append(delta_1)
                deltas_h.append(delta_h)
            g_w_0 = np.dot(delta_0,as_1[-1].T)
            g_b_0 = delta_0
            sum_g_w_0 = g_w_0+sum_g_w_0
            sum_g_b_0 = g_b_0+sum_g_b_0
            deltas_1.reverse()
            deltas_h.reverse()
            g_w_1 = np.zeros_like(w_1)
            g_w_h = np.zeros_like(w_h)
            g_b_1 = np.zeros_like(b_1)
            g_b_h = np.zeros_like(b_h)
            for x_m,a,delta_1,delta_h in zip(x_mat,as_1[:-1],deltas_1,delta_h):
                g_w_1 = np.dot(delta_1,x_m.T)+g_w_1
                g_b_1 = delta_1 + g_b_1
                g_w_h =  np.dot(delta_h,a.T)+g_w_h
                g_b_h = delta_h + g_b_h

            sum_g_w_1 = g_w_1+sum_g_w_1
            sum_g_b_1 = g_b_1+sum_g_b_1
            sum_g_w_h = g_w_h+sum_g_w_h
            sum_g_b_h = g_b_h+sum_g_b_h

        w_0 = (1-eta*lamb)*w_0-eta*(sum_g_w_0/len(mini_x))  
        w_1 = (1-eta*lamb)*w_1-eta*(sum_g_w_1/len(mini_x))  
        w_h = (1-eta*lamb)*w_h-eta*(sum_g_w_h/len(mini_x))  
        b_0 = b_0-eta*(sum_g_b_0/len(mini_x))
        b_1 = b_1-eta*(sum_g_b_1/len(mini_x))
        b_h = b_h-eta*(sum_g_b_h/len(mini_x))

    print("epoch {0}:{1}/{2}".format(k,evaluate(test_data_x,test_data_y
          ,w_0,w_1,w_h,b_0,b_1,b_h),len(test_data_x)))    
        
        
            
                
                
               
               
               
            
            