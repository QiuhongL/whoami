# -*- coding: utf-8 -*-
"""
Created on Sat Aug 5 10:09:02 2018

@author: Lai
"""

import tensorflow as tf
import numpy as np
import math
import collections
import os
import pickle as pkl
import re
import shutil
'''
train a word2vec model in tensorflow
using skip-gram + negative sampling 
'''
        
class word2vec():
    def __init__(self,
                 vocab_list = None, 
                 embedding_size = 200,
                 win_size = 3,
                 num_sampled = 1000,
                 learning_rate = 1.0,
                 logdir = '/tmp/word2vec',
                 model_path = None,
                 pre_train= None
                 ):
        self.pre_train = pre_train
        self.batch_size = None # number of word pairs
        if model_path!=None:
            self.load_model(model_path)
        else:
            # model parameters
            assert type(vocab_list)==list 
            self.vocab_list     = vocab_list # vocabulary wanted to train
            self.vocab_size     = vocab_list.__len__() 
            self.embedding_size = embedding_size # word vector dimension
            self.win_size        = win_size # window size
            self.num_sampled    = num_sampled # number of nce samples
            self.learning_rate  = learning_rate # learning rate
            self.logdir         = logdir 
            self.min_loss = 1000
            self.word2id = {}   # word => id 
            for i in range(self.vocab_size):
                self.word2id[self.vocab_list[i]] = i


            self.train_words_num = 0 # number of words had been trained
            self.train_sents_num = 0 # number of sentences had been trained
            self.train_times_num = 0 # iteration times 

            self.train_loss_records = collections.deque(maxlen=10) # record loss in last 10 iterations
            self.train_loss_k10 = 0 

            self.build_graph()
            self.init_op()
  
        
    def init_op(self): # initialization
        self.sess = tf.Session(graph = self.graph)
        self.sess.run(self.init)
        self.summary_writer = tf.summary.FileWriter(self.logdir, self.sess.graph)
        
        
    def build_graph(self): # build a tensor graph but not calculate
        self.graph = tf.Graph()
        with self.graph.as_default():
            # define placeholers and variables
            self.train_inputs = tf.placeholder(tf.int32, shape=[self.batch_size],name='train_id') # center word id
            self.train_labels = tf.placeholder(tf.int32, shape=[self.batch_size, 1],name='train_labels') # context word id
            if self.pre_train is not None:
                self.embedding_dict = tf.Variable(tf.convert_to_tensor(self.pre_train,dtype=tf.float32),name='word_vectors')
            else:
                self.embedding_dict = tf.Variable(
                        tf.random_uniform([self.vocab_size,self.embedding_size],-1.0,1.0),name='word_vectors' 
                        ) # initialize
            
            self.nce_weight = tf.Variable(tf.truncated_normal([self.vocab_size, self.embedding_size],
                                                              stddev=1.0/math.sqrt(self.embedding_size)),name='nce_w')
            self.nce_biases = tf.Variable(tf.zeros([self.vocab_size]),name='nce_b' )
            # ids => vectors
            with tf.name_scope('embed'):
                embed = tf.nn.embedding_lookup(self.embedding_dict, self.train_inputs)
            
            with tf.name_scope('loss'):
                self.loss = tf.reduce_mean(
                        tf.nn.nce_loss(
                                weights = self.nce_weight,
                                biases = self.nce_biases,
                                labels = self.train_labels,
                                inputs = embed,
                                num_sampled = self.num_sampled,
                                num_classes = self.vocab_size
                                )
                        )
                        
            # tensorboard 
            tf.summary.scalar('loss',self.loss)  

            # train with gradient descent 
            with tf.name_scope('train'):
                self.train_op = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(self.loss)  
                
            # calculate similarity
            with tf.name_scope('similarity'):
                self.test_word_id = tf.placeholder(tf.int32,shape=[None])
                vec_l2_model = tf.sqrt(  # L2 morn
                                       tf.reduce_sum(tf.square(self.embedding_dict),1,keep_dims=True)
                                       )

                avg_l2_model = tf.reduce_mean(vec_l2_model)
                tf.summary.scalar('avg_vec_model',avg_l2_model)

                self.normed_embedding = self.embedding_dict / vec_l2_model
                # embedding regurilazation
                test_embed = tf.nn.embedding_lookup(self.normed_embedding, self.test_word_id)
                self.similarity = tf.matmul(test_embed, self.normed_embedding, transpose_b=True)

            # initialization
            with tf.name_scope('init'):
                self.init = tf.global_variables_initializer()

            self.merged_summary_op = tf.summary.merge_all()

            self.saver = tf.train.Saver()
            
    # train word vectors        
    def train_operation(self, input_sentence):
        batch_inputs = [] # center word id
        batch_labels = [] # context word id
        
        for sent in input_sentence:
            for i in range(sent.__len__()): # split sentences into word pairs                
                start = max(0,i-self.win_size)
                end = min(sent.__len__(),i+self.win_size+1)
                for index in range(start,end):
                    if index == i:
                        continue # skip the center word
                    else:
                        input_id = self.word2id.get(sent[i])
                        label_id = self.word2id.get(sent[index])
                        if not (input_id and label_id):
                            continue
                        batch_inputs.append(input_id)
                        batch_labels.append(label_id) 
        if len(batch_inputs)==0:
            return
        batch_inputs = np.array(batch_inputs,dtype=np.int32)
        batch_labels = np.array(batch_labels,dtype=np.int32)
        batch_labels = np.reshape(batch_labels,[batch_labels.__len__(),1])
        feed_dict = {
            self.train_inputs: batch_inputs,
            self.train_labels: batch_labels,
        }
        
        _,loss_val, summary_str = self.sess.run([self.train_op,self.loss,self.merged_summary_op], feed_dict=feed_dict)
        
        self.train_loss_records.append(loss_val) # record loss in last 10 iterations
        self.train_loss_k10 = np.mean(self.train_loss_records)
        if self.train_sents_num % 1000 == 0 : # check each 1000 iterations
            if self.train_loss_k10 < self.min_loss:
                self.min_loss = self.train_loss_k10
            self.saver.save(self.sess,self.logdir+'/model_tf/w2v',self.train_sents_num)
            self.summary_writer.add_summary(summary_str,self.train_sents_num) # tensorboard
            print("{a} sentences dealed, loss: {b}"
                  .format(a=self.train_sents_num,b=self.train_loss_k10))

        self.train_words_num += batch_inputs.__len__()
        self.train_sents_num += input_sentence.__len__()
        self.train_times_num += 1 
    
    # calculate similarity among certain words and return top k most similar words   
    def cal_similarity(self,test_word_id_list,top_k=10):
        sim_matrix = self.sess.run(self.similarity, feed_dict={self.test_word_id:test_word_id_list})
        sim_mean = np.mean(sim_matrix)
        sim_var = np.mean(np.square(sim_matrix-sim_mean))
        test_words = []
        near_words = []
        for i in range(test_word_id_list.__len__()):
            test_words.append(self.vocab_list[test_word_id_list[i]])
            nearst_id = (-sim_matrix[i,:]).argsort()[1:top_k+1]
            nearst_word = [self.vocab_list[x] for x in nearst_id]
            near_words.append(nearst_word)
        return test_words,near_words,sim_mean,sim_var
    
    # load a model had already trained
    def load_model(self,model_path):
        name_list = os.listdir(model_path+'/model_tf/')
        num_list = []
        for name in name_list:
            num = re.search('(?<=-)([0-9]+)(?<!\.)',name)
            if num and (num.group() not in num_list):
                num_list.append(int(num.group()))
        tf_file_name = max(num_list)    
        model_tf_path = model_path+'/model_tf/w2v-'+str(tf_file_name)
        model_para_path = model_path+'/model_param/params.pkl'
        if not os.path.exists(model_para_path):
            raise RuntimeError('params file not exists')
        with open(model_para_path,'rb') as f:
            model = pkl.load(f)
            self.vocab_list = model['vocab_list']
            self.vocab_size = model['vocab_size']
            self.logdir = model['logdir']
            self.word2id = model['word2id']
            self.embedding_size = model['embedding_size']
            self.learning_rate = model['learning_rate']
            self.win_size = model['win_size']
            self.num_sampled = model['num_sampled']
            self.train_words_num = model['train_words_num']
            self.train_sents_num = model['train_sents_num']
            self.train_times_num = model['train_times_num']
            self.train_loss_records = model['train_loss_records']
            self.train_loss_k10 = model['train_loss_k10']
            self.min_loss = model['min_loss']
        if not os.path.exists(model_tf_path+'.meta'):
            raise RuntimeError('tf file not exists')
        self.build_graph()
        self.init_op()
        saver  = tf.train.import_meta_graph(model_tf_path+'.meta')
        saver.restore(self.sess,model_tf_path)
        
    def save_model(self):
        save_path = self.logdir+'/model_param'
        if os.path.exists(save_path):
            shutil.rmtree(save_path)
        os.mkdir(save_path)

        # record model parameters
        model = {}
        var_names = ['vocab_size',            
                     'vocab_list',      
                     'learning_rate',
                     'word2id',         
                     'embedding_size', 
                     'logdir',          
                     'win_size',         
                     'num_sampled',    
                     'train_words_num',      
                     'train_sents_num', 
                     'train_times_num', 
                     'train_loss_records',  
                     'train_loss_k10', 
                     'min_loss'
                     ]
        for var in var_names:
            model[var] = eval('self.'+var)
#        param_path = os.path.join(self.logdir+'/model_para/params.pkl')
        if os.path.exists(save_path+'/params.pkl'):
            os.remove(save_path+'/params.pkl')
        with open(save_path+'/params.pkl','wb') as f:
            pkl.dump(model,f)
#                                                
