# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 23:25:08 2017

@author: Lai
"""
import numpy as np
import random
from PIL import Image

###读入数据
xdigit_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\azip.dat','r')
ydigit_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\dzip.dat','r')
test_y_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\dtest.dat','r')
test_x_f = open(r'C:\Users\Administrator\Desktop\MARIMO\统计计算R\手写体识别\testzip.dat','r')
xdigit = xdigit_f.readlines()
ydigit = ydigit_f.readlines()
test_y = test_y_f.readlines()
test_x = test_x_f.readlines()
xdigit_f.close()
ydigit_f.close()
test_x_f.close()
test_y_f.close()

###将数据存为数组
x_tr0 =  np.zeros((1707,256)) 
i = 0
for line in xdigit:
    li =line.strip(' ').split('  ')
    x_tr0[:,i] = li
    i+=1

y_tr0 =np.zeros(1707)
i = 0
yli = ydigit[0].strip(' ').split('  ')
for yr in yli:
    y_tr0[i] = int(float(yr))
    i += 1

x_te0 =  np.zeros((2007,256))
i = 0
for line in test_x:
    li =line.strip(' ').split('  ')
    x_te0[:,i] = li
    i+=1
    
y_te0 =np.zeros(2007)
i = 0
yli = test_y[0].strip(' ').split('  ')
for yr in yli:
    y_te0[i] = int(float(yr))
    i += 1
    
###对训练集和测试集数据个数进行调整，将数据均分为6份，1份测试集，剩下为训练集，
#以便于后期进行交叉验证
x_tr = np.vstack((x_tr0,x_te0))[range(3095),:]#按行拼接
x_te = np.vstack((x_tr0,x_te0))[3095:3714,:]
y_tr = np.vstack((y_tr0.reshape(1707,1),y_te0.reshape(2007,1)))[range(3095),0]
y_te = np.vstack((y_tr0.reshape(1707,1),y_te0.reshape(2007,1)))[3095:3714,0]

del i, li, line, yr, yli, test_x, test_y, xdigit, ydigit 
del x_tr0, y_tr0, x_te0, y_te0

my_im = Image.fromarray(x_tr[10,:].reshape(16,16).astype(np.uint8))
my_im.show()#随便画一张图看看，这里用第11张

###定义knn类
class nearestneighbor():
    def train(self,x,y):
        self.x_tr = x
        self.y_tr = y
        
        
    def predict(self,x,y,k = 1):
        num_x_te  = x.shape[0]
        ypred  = np.zeros(num_x_te)
        dists = np.zeros((num_x_te, self.x_tr.shape[0]))
        for i in range(num_x_te):
            dists[i,:] = np.sum((self.x_tr - x[i])**2, 1)
            closest_y = self.y_tr[np.argsort(dists[i])[0:k]].astype('int64')
            
            ypred[i] = np.bincount(closest_y).argmax()
        return sum(ypred == y)
            

###交叉验证函数
def cross_validation(x,y):
    n = int(x.shape[0]/5)
    acc = np.zeros((5,9))
    ks = [1,3,5,7,9,10,20,50]
    for i in range(5):
        x_val = x[i*n:(i+1)*n,:]#训练集均分为1份，每份轮流作为验证集
        y_val = y[i*n:(i+1)*n]
        x_tr1 = x[0:i*n,:]
        y_tr1 = y[0:i*n]
        x_tr2 = x[(i+1)*n:,:]
        y_tr2 = y[(i+1)*n:]
        x_tr = np.vstack((x_tr1,x_tr2))
        y_tr = np.vstack((y_tr1.reshape(y_tr1.shape[0],1),
                         y_tr2.reshape(y_tr2.shape[0],1)))[:,0]
        for k,j in zip(ks,range(9)):
            nn = nearestneighbor()
            nn.train(x_tr,y_tr)
            acc[i,j] = nn.predict(x_val,y_val,k)
    best_k = ks[np.argmax(np.mean(acc,0))] 
    print(np.mean(acc,0))#不同k值验证数据集给出的平均正确个数       
    print('the best k is {0}'.format(best_k) )    

cross_validation(x_tr,y_tr)#进行交叉验证，寻找最优的k值

###最优k值为1,对真正的测试集进行预测
knn = nearestneighbor()
knn.train(x_tr,y_tr)
knn.predict(x_te,y_te,1)#测试集数据个数为619，正确预测个数为581 
    
            
            
        
        
    
          
        