# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 10:01:05 2018

@author: Lai
"""
import numpy as np
import time
import copy

class mysolve():
    '''    此类用于解线性方程Ax = b
    输入 A:数组 b:数组
    cholesky_decom方法对系数矩阵进行分解,返回L
    forward_and_back_solve方法先forwardsolve求解L*d = b
    解出d以后用backsolve进一步求解L.T*x = d
    x即为线性方程组的解'''
    def __init__(self, A, b):
        self.A = copy.copy(A)
        #print(id(self.A))
        self.b = copy.copy(b)
        
    def cholesky_decom(self):
        if self.A.shape[0] != self.A.shape[1]:
            print('Wrong dimensions of matrix A!')
                    
        elif np.sum(self.A.T != self.A)>0:
            print('Input matrix is not symmetrical!')
            
        else:
            L = np.zeros(self.A.shape)
            for i in range(self.A.shape[0]):
                L[i, i] = np.sqrt(self.A[i, i])
                L[i+1:, i] = self.A[i+1:, i]/L[i, i] 
                self.A[i+1:, i+1:] -= np.dot(L[i+1:, i:i+1], [L[i+1:, i]])
            print('Cholesky decomposition result:')
            print(L)
            
            return L     
    
    def forward_and_back_solve(self):
        if self.A.shape[0] != self.b.shape[0]:
            print('Wrong dimensions of matrix A or vector b!')
            
        else:
            time_start = time.time()
            L = self.cholesky_decom()
            L_T = L.T
            d = np.zeros(self.b.shape)#forwardsolve
            for i in range(self.b.shape[0]):
                d[i] = self.b[i]/L[i, i]
                if i<self.b.shape[0]-1:
                    self.b[i+1:] = self.b[i+1:] - d[i]*L[i+1:, i]           
            x = np.zeros(self.b.shape)#backsolve
            for i in range(self.b.shape[0]-1, -1, -1):
                x[i] = d[i]/L_T[i,i]
                if i>0:
                    d[:i] = d[:i] - x[i]*L_T[:i, i]
            print('The result:')
            print(x)
            print('Tne code run {} s'.format(time.time()-time_start))
            return x    
        
    
   

################################example
print(mysolve.__doc__)
#np.random.seed(1)
x1 = np.random.normal(size = 20).reshape((5,4))
A1 = np.dot(x1.T, x1)
b1 = np.array([1.0,2.0,3.0,4.0])
a1 = mysolve(A1, b1)
x = a1.forward_and_back_solve()
np.dot(A1, x)
b1

    