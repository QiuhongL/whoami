# -*- coding: utf-8 -*-
"""
Created on Thu Jul  29 20:38:07 2018

@author: Lai
"""

import tensorflow as tf
import numpy as np
import os
import os.path
import pickle as pkl
import re
'''
multi-lstm model
'''

class multi_lstm():
    def __init__(self,
                 time_step = 20,
                 input_size = 100,
                 output_size = 1,
                 cell_size = 10,
                 cell_num = 3,
                 batch_size = 50,
                 keep_prob = 0.75,
                 model_path=None,
                 logdir='/log/lstm',
                 ad = False):
        if model_path is not None:
            self.load_model(model_path)
        else:
            self.time_step = time_step # time step
            self.input_size = input_size # input size
            self.output_size = output_size # output size 
            self.batch_size = batch_size # batch size
            self.cell_size = cell_size # cell size/unit size
            self.cell_num = cell_num # number of cells
            self.logdir = logdir
            self.keep_prob = keep_prob # drop out
            self.train_times = 0 # iteration time
            self.state = None
            self.ad = ad
#            self.lstmcell()
            self.build_graph()
            self.init_op()
    # initialization        
    def init_op(self):    
        self.sess = tf.Session(graph=self.graph)
        self.sess.run(self.init)
        self.summary_writer = tf.summary.FileWriter(self.logdir,self.sess.graph)
    # build a tensor graph but not calculate
    def build_graph(self):
        self.graph = tf.Graph()
        with self.graph.as_default():
			# define placeholders 
            with tf.name_scope('inputs'):
                self.lr = tf.placeholder(tf.float32,name='lr')
                self.x = tf.placeholder(tf.float32, [None, self.time_step, self.input_size],name='x')
                self.labels = tf.placeholder(tf.float32, [None, self.output_size], name='labels')
                self.x_2d = tf.reshape(self.x,[-1,self.input_size])
            
			
			# inputs and the frist layer
            with tf.variable_scope('input_layer'):
                self.input_weight = tf.Variable(tf.truncated_normal([self.input_size, self.cell_size]), name='input_w')
                self.input_biases = tf.Variable(tf.zeros([self.cell_size]), name='input_b')
                with tf.name_scope('wx_plus_b'):
                    wx_plus_b = tf.matmul(self.x_2d,self.input_weight)+self.input_biases
                self.in_to_cell = tf.reshape(wx_plus_b, [-1, self.time_step, self.cell_size],name='in_to_cell')
				
            # lstm cells    
            with tf.variable_scope('lstm_cells'):
                self.multi_lstm_cell = tf.contrib.rnn.MultiRNNCell(
                        [self.lstmcell() for i in range(self.cell_num)], state_is_tuple=True
                        )
                with tf.name_scope('init_state'):
                    self.init_state = self.multi_lstm_cell.zero_state(self.batch_size, tf.float32)
                self.cell_outputs,self.final_state = tf.nn.dynamic_rnn(
                        self.multi_lstm_cell, inputs=self.in_to_cell, initial_state=self.init_state, time_major=False
                        ) 
            #    self.to_pred = tf.reshape(tf.concat(cell_outputs,1), [-1, self.cell_size])
                self.to_pred = self.cell_outputs[:, -1, :]
            with tf.variable_scope('outputs_layer'):
                self.output_weight = tf.Variable(tf.truncated_normal([self.cell_size, self.output_size]), name='output_w')
                self.output_biases = tf.Variable(tf.zeros([self.output_size]), name='output_b')
                if self.ad:
                    self.ad1 = tf.Variable(tf.zeros([1]), name = 'adjust1')
                    self.ad2 = tf.Variable(tf.ones([1]), name = 'adjust2')  
                    with tf.name_scope('wx_plus_b'):
                        self.pred = self.ad1*tf.reshape(tf.matmul(self.to_pred, self.output_weight)+self.output_biases,[-1])+self.ad2*tf.reshape(self.x[:,-1,0],[-1])
                else:    
                    with tf.name_scope('wx_plus_b'):
                        self.pred = tf.matmul(self.to_pred, self.output_weight)+self.output_biases										
            # define loss funtion        
            with tf.name_scope('loss'):
                self.loss = tf.reduce_mean(tf.square(tf.reshape(self.pred,[-1])-tf.reshape(self.labels, [-1])))
                
            tf.summary.scalar('loss',self.loss) # tensorboard
            
			# train operation
            with tf.name_scope('train'):
                self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss)
				
            # initialization    
            with tf.name_scope('init'):
                self.init = tf.global_variables_initializer()
                
            self.merged_summary_op = tf.summary.merge_all()
            self.saver = tf.train.Saver()    
                
    # train model
    def train_operation(self,batch_inputs,batch_labels,lr):
        if self.train_times == 0:
            feed_dict = {
                    self.x:batch_inputs,
                    self.labels:batch_labels,
                    self.lr:lr
                    }
        else:
            feed_dict = {
                    self.x:batch_inputs,
                    self.labels:batch_labels,
                    self.init_state:self.state,
                    self.lr:lr
                    }
        _,loss_val,self.state,summary_str=self.sess.run([self.train_op,self.loss,self.final_state,self.merged_summary_op],feed_dict=feed_dict)
        if self.train_times%300==0:# save each 300 iterations
            self.saver.save(self.sess,self.logdir+'/model_tf/lstm',self.train_times)
            self.summary_writer.add_summary(summary_str, self.train_times)
            with open(self.logdir+'/loss_record.txt','a',encoding='utf-8') as f:
                print("Batch: {a}, loss: {b}".format(a=self.train_times,b=loss_val),file = f)
            
        self.train_times += 1
    
    def lstmcell(self):
        lstm_cell = tf.contrib.rnn.BasicLSTMCell(num_units=self.cell_size, forget_bias=1.0, state_is_tuple=True)
        lstm_cell = tf.contrib.rnn.DropoutWrapper(cell=lstm_cell, input_keep_prob=1.0, output_keep_prob=self.keep_prob)
        return lstm_cell
    
	# prediction
    def predict(self,data):
        data = np.reshape(data,[-1,self.time_step, self.input_size])
        feed_dict = {self.x:data}
        pred_val = self.sess.run([self.pred],feed_dict=feed_dict)
        pred_val = np.reshape(pred_val,[-1])
        return pred_val
    
	# save model parameters
    def save_model(self):
        save_path = self.logdir+'/model_param'
        if not os.path.exists(save_path):
            os.mkdir(save_path)
        model = {}
        var_names = ['time_step',
                     'input_size',
                     'output_size',
                     'cell_size',
                     'cell_num',
                     'batch_size',
                     'keep_prob',
                     'logdir',
                     'train_times' ,
                     'state',
                     'ad'
                     ]
        for var in var_names:
            model[var] = eval('self.'+var)
        if os.path.exists(save_path+'/params.pkl'):
            os.remove(save_path+'/params.pkl')
        with open(save_path+'/params.pkl','wb') as f:
            pkl.dump(model,f) 
			
    # load a model
    def load_model(self,model_path):
        name_list = os.listdir(model_path+'/model_tf/')
        num_list = []
        for name in name_list:
            num = re.search('(?<=-)([0-9]+)(?<!\.)',name)
            if num and (num.group() not in num_list):
                num_list.append(int(num.group()))
        tf_file_name = max(num_list)                    
        model_tf_path = model_path+'/model_tf/lstm-'+str(tf_file_name)
        model_para_path = model_path+'/model_param/params.pkl'
        if not os.path.exists(model_para_path):
            raise RuntimeError('params file not exists')
        with open(model_para_path,'rb') as f:
            model = pkl.load(f)
            self.time_step = model['time_step']
            self.input_size = model['input_size']
            self.output_size = model['output_size']
            self.cell_size = model['cell_size']
            self.cell_num = model['cell_num']
            self.batch_size = model['batch_size']
            self.keep_prob = model['keep_prob']
            self.logdir = model['logdir']
            self.train_times = model['train_times']    
            self.state = model['state']
            self.ad = model['ad']
        if not os.path.exists(model_tf_path+'.meta'):
            raise RuntimeError('tf file not exists')
        self.build_graph()
        self.init_op()
        saver  = tf.train.import_meta_graph(model_tf_path+'.meta')
        saver.restore(self.sess,model_tf_path)
