# -*- coding: utf-8 -*-
"""
Created on Sun Dec 10 10:16:34 2017

@author: Lai
"""
from sklearn.preprocessing import Imputer
import numpy as np
import random
import tensorflow as tf
import matplotlib.pyplot as plt

##############################
#数据读入与处理
##############################
f = open('ad.data')
f_read = f.readlines()
f.close()
x00 =  np.zeros((3279,1558),dtype = np.float32)
#3279条数据，1558个变量
y0 = np.ones((3279,2),dtype = np.float32)
#标签二分变量：第一列表示是否是广告，第二列表示是否不是广告
i = 0
for line in f_read:#将数据转存为数组，用nan对缺失值进行编码
    li = line.replace('?,','nan,').strip(' ').split(',')
    x00[i] = li[:-1]
    if 'non' in li[-1]:
        y0[i,0] = 0
    else:
        y0[i,1] = 0
    i+=1
imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
#用每个变量的均值填补缺失值
imp.fit(x00)
x0 = imp.transform(x00)
sum(y0[:,0])#459条数据是广告
3279*0.7#70%的数据作为训练样本
459*0.7
321/2295#训练集中广告数量占比
138/984#测试集中广告数量占比
x_tr_ad = x0[:321]
y_tr_ad = y0[:321]
x_tr_nonad = x0[459:459+1974]
y_tr_nonad = y0[459:459+1974]
x_te_ad = x0[321:459]
y_te_ad = y0[321:459]
x_te_nonad = x0[459+1974:]
y_te_nonad = y0[459+1974:]
x_tr = np.vstack((x_tr_ad,x_tr_nonad))
y_tr = np.vstack((y_tr_ad,y_tr_nonad))
x_te = np.vstack((x_te_ad,x_te_nonad))
y_te = np.vstack((y_te_ad,y_te_nonad))


################################
#描述分析
################################
fig1 = plt.figure()
ax1 = fig1.add_subplot(1,1,1)
ax1.boxplot([np.log(x_tr_nonad[:,0]),np.log(x_tr_ad[:,0])],labels = ('nonad','ad'))
ax1.set_ylabel('log(height)')
plt.show()#对高度花分组箱线图
fig2 = plt.figure()
ax2 = fig2.add_subplot(1,1,1)
ax2.boxplot([np.log(x_tr_nonad[:,1]),np.log(x_tr_ad[:,1])],labels = ('nonad','ad'))
ax2.set_ylabel('log(width)')
plt.show()#对宽度花分组箱线图
fig3 = plt.figure()
ax3 = fig3.add_subplot(1,1,1)
ax3.boxplot([np.log(x_tr_nonad[:,2]),np.log(x_tr_ad[:,2])],labels = ('nonad','ad'))
ax3.set_ylabel('log(aratio)')
plt.show()#对宽高比率画分组箱线图
fig4 = plt.figure()
ax4 = fig4.add_subplot(1,1,1)
y1 = np.zeros(2)
y2 = np.zeros(2)
y1[0] = sum(x_tr_nonad[:,3]==1)
y2[0] = sum(x_tr_nonad[:,3]==0)
y1[1] = sum(x_tr_ad[:,3]==1)
y2[1] = sum(x_tr_ad[:,3]==0)
x1 = [0,1]
plt.bar(x1,+y1,width=0.8,facecolor="#9999ff",edgecolor="white",label = ('nonad','ad'))  
plt.bar(x1,-y2,facecolor="#ff9999",edgecolor="white")  
plt.text(x1[0],y1[0],'nonad-local',ha='center',va = 'bottom') 
plt.text(x1[1],y1[1],'ad-local',ha='center',va = 'bottom')  
plt.text(x1[0],-(y2[0]),'nonad-nonlocal',ha='center',va='top')
plt.text(x1[1],-(y2[1]),'ad-nonlocal',ha='center',va='top') #是否广告与是否同一域名堆积条形图 

x_tr[0] #非广告


################################
#卷积神经网络
################################
######################
#3层卷积层+dropout
######################

x = tf.placeholder(tf.float32, [None, 38,41,1])
y_ = tf.placeholder(tf.float32, [None, 2])
lr = tf.placeholder(tf.float32)
pkeep = tf.placeholder(tf.float32)
w1 = tf.Variable(tf.truncated_normal([3,3,1,10] ,stddev=0.1))
b1 = tf.Variable(tf.ones([10])/10)
w2 = tf.Variable(tf.truncated_normal([3,3,10,20] ,stddev=0.1))
b2 = tf.Variable(tf.ones([20])/10)
w3 = tf.Variable(tf.truncated_normal([3,3,20,30] ,stddev=0.1))
b3 = tf.Variable(tf.ones([30])/10)
w4 = tf.Variable(tf.truncated_normal([30*38*41,30] ,stddev=0.1))
b4 = tf.Variable(tf.zeros([30]))
w5 = tf.Variable(tf.truncated_normal([30,2] ,stddev=0.1))
b5 = tf.Variable(tf.zeros([2]))
test_data={x: x_te, y_: y_te}

stride = 1
with tf.variable_scope('conv0'):#网络结构
    a1 = tf.nn.relu(tf.nn.conv2d(x, w1, strides=[1, stride, stride, 1], 
                                 padding='SAME') + b1)    
    a1d = tf.nn.dropout(a1,pkeep)
with tf.variable_scope('conv1'):
    a2 = tf.nn.relu(tf.nn.conv2d(a1d, w2, strides=[1, stride, stride, 1],
                                 padding='SAME') + b2)
    a2d = tf.nn.dropout(a2,pkeep)
with tf.variable_scope('conv2'):
    a3 = tf.nn.relu(tf.nn.conv2d(a2d, w3, strides=[1, stride, stride, 1], 
                                 padding='SAME') + b3)
    a3d = tf.nn.dropout(a3,pkeep)
with tf.variable_scope('fc'):
    yy = tf.reshape(a3d, shape=[-1, 38*41*30])
    a4 = tf.nn.relu(tf.matmul(yy, w4) + b4)
    a4d = tf.nn.dropout(a4,pkeep)
    ylogits = tf.matmul(a4d, w5) + b5
    y = tf.nn.softmax(ylogits)

cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=ylogits, 
                                                        labels=y_)
cross_entropy = tf.reduce_mean(cross_entropy)*100
train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)
is_correct = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))

n = 10
epoch = 100
lrmin = 0.0001
lrmax = 0.003
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)
visual_a = np.zeros(epoch)#储存返回结果
visual_c = np.zeros(epoch)


for k in range(epoch):
    #k=0
    index = [i for i in range(x_tr.shape[0])]
    random.shuffle(index)
    l_r = lrmin+(lrmax-lrmin)*np.exp(-k/epoch)
    for i in range(int(x_tr.shape[0]/n)):
        #i=0
        mini_x = x_tr[index[i*n:(i+1)*n]].reshape(-1,38,41,1)
        mini_y = y_tr[index[i*n:(i+1)*n]]
        train_data={x: mini_x, y_: mini_y,lr: l_r,pkeep:0.75}
        test_data={x: x_te.reshape(-1,38,41,1), y_: y_te,pkeep:1.0}

        sess.run(train_step,train_data)
    c = sess.run(cross_entropy,train_data)    
    a = sess.run(accuracy,test_data) 
    visual_a[k] = a
    visual_c[k] = c
    print("epoch {0}:{1}".format(k,a))
np.savetxt('a_conv_d_3.out',visual_a,delimiter=",") 
np.savetxt('c_conv_d_3.out',visual_c,delimiter=",")

#########################
#3层卷积层+batch normalization
#########################
def bn_layer(input_layer):#批标准化
    
    dim = input_layer.get_shape().as_list()[-1]
    mean, var = tf.nn.moments(input_layer, list(range(len(input_layer.shape) - 1)) 
    ,keep_dims=True)#计算均值，方差
    beta = tf.get_variable('beta', dim, 
                           initializer=tf.constant_initializer(0.0, tf.float32))
    gamma = tf.get_variable('gamma', dim,
                            initializer=tf.constant_initializer(1.0, tf.float32))
    out = tf.nn.batch_normalization(input_layer, mean, var, beta, gamma, 0.001)
    
    return out

x = tf.placeholder(tf.float32, [None, 38,41,1])
y_ = tf.placeholder(tf.float32, [None, 2])
lr = tf.placeholder(tf.float32)

w1 = tf.Variable(tf.truncated_normal([3,3,1,10] ,stddev=0.1))
w2 = tf.Variable(tf.truncated_normal([3,3,10,20] ,stddev=0.1))
w3 = tf.Variable(tf.truncated_normal([3,3,20,30] ,stddev=0.1))
w4 = tf.Variable(tf.truncated_normal([30*38*41,30] ,stddev=0.1))
w5 = tf.Variable(tf.truncated_normal([30,2] ,stddev=0.1))
test_data={x: x_te, y_: y_te}

stride = 1
with tf.variable_scope('conv0'):
    a1 = tf.nn.relu(tf.nn.conv2d(x, w1, strides=[1, stride, stride, 1], 
                                 padding='SAME'))    
    bn_a1 = bn_layer(a1)
with tf.variable_scope('conv1'):
    a2 = tf.nn.relu(tf.nn.conv2d(bn_a1, w2, strides=[1, stride, stride, 1],
                                 padding='SAME'))
    bn_a2 = bn_layer(a2)
with tf.variable_scope('conv2'):
    a3 = tf.nn.relu(tf.nn.conv2d(bn_a2, w3, strides=[1, stride, stride, 1], 
                                 padding='SAME'))
    bn_a3 = bn_layer(a3)
with tf.variable_scope('fc'):
    yy = tf.reshape(bn_a3, shape=[-1, 38*41*30])
    a4 = tf.nn.relu(tf.matmul(yy, w4))
    bn_a4 = bn_layer(a4)
    ylogits = tf.matmul(bn_a4, w5) 
    y = tf.nn.softmax(ylogits)

cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=ylogits, 
                                                        labels=y_)
cross_entropy = tf.reduce_mean(cross_entropy)*100
train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)
is_correct = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))

n = 10
epoch = 100
lrmin = 0.0001
lrmax = 0.003
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)
visual_a = np.zeros(epoch)
visual_c = np.zeros(epoch)


for k in range(epoch):
    #k=0
    index = [i for i in range(x_tr.shape[0])]
    random.shuffle(index)
    l_r = lrmin+(lrmax-lrmin)*np.exp(-k/epoch)
    for i in range(int(x_tr.shape[0]/n)):
        #i=0
        mini_x = x_tr[index[i*n:(i+1)*n]].reshape(-1,38,41,1)
        mini_y = y_tr[index[i*n:(i+1)*n]]
        train_data={x: mini_x, y_: mini_y,lr: l_r}
        test_data={x: x_te.reshape(-1,38,41,1), y_: y_te}

        sess.run(train_step,train_data)
    c = sess.run(cross_entropy,train_data)    
    a = sess.run(accuracy,test_data) 
    visual_a[k] = a
    visual_c[k] = c
    print("epoch {0}:{1}".format(k,a))
np.savetxt('a_conv_bn_3.out',visual_a,delimiter=",") 
np.savetxt('c_conv_bn_3.out',visual_c,delimiter=",")    
########################
#10层卷积神经网络&20层卷积神经网络
########################
def bn_relu_conv_layer(input_layer, filter_shape, stride = 1, padding = 'SAME'):

    bn = bn_layer(input_layer)
    relu = tf.nn.relu(bn)
    filter = tf.get_variable('filter',filter_shape,
                             initializer=tf.truncated_normal_initializer(0,0.1))
    out = tf.nn.conv2d(relu, filter, [1,stride,stride,1], padding = padding)
    
    return out

def inference(n,x):#网络结构
    
    layers = []
    with tf.variable_scope('conv0'):
        filter = tf.get_variable('filter', [3, 3, 1, 10],
                             initializer=tf.truncated_normal_initializer(0,0.1)) 
        conv0 = tf.nn.conv2d(x, filter, [1, 1, 1, 1], padding='SAME')
        layers.append(conv0)
        
    for i in range(n-2):
        with tf.variable_scope('conv1_%d' %i):
            conv1 = bn_relu_conv_layer(layers[-1], [3, 3, 10, 10])
            layers.append(conv1)
            
    with tf.variable_scope('conv2'):
        conv2 = bn_relu_conv_layer(layers[-1], [3, 2, 10, 10], padding='VALID')
        print(conv2.shape)
        layers.append(conv2)
        
    with tf.variable_scope('fc'):
        bn = bn_layer(layers[-1])
        relu = tf.nn.relu(bn)
        fc_ = tf.nn.max_pool(relu,[1,2,2,1],[1,2,2,1],padding='VALID')
        fc_in = tf.reshape(fc_, shape=[-1, 18*20*10])
        print(fc_.shape)
        fc_w = tf.get_variable('fc_w', [18*20*10,2],
                               initializer=tf.truncated_normal_initializer(0,0.1))
        logits = tf.nn.relu(tf.matmul(fc_in, fc_w))
        
    return logits

x = tf.placeholder(tf.float32, [None, 38,41,1])
y_ = tf.placeholder(tf.float32, [None, 2])
lr = tf.placeholder(tf.float32)

    


n = 15#每个迷你批包含的数据数量
epoch = 100#周期
lrmin = 0.0001#最小学习速率
lrmax = 0.006#最大学习速率

test_data={x: x_te.reshape(-1,38,41,1), y_: y_te}
logits = inference(10,x)#20层将10改为20即可
y = tf.nn.softmax(logits)
cross_en = tf.nn.softmax_cross_entropy_with_logits(logits=logits, 
                                                   labels=y_)#损失函数使用交叉熵函数
cross_entropy = tf.reduce_mean(cross_en)

train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)#使用比随机梯度下降效果更好的adam
is_correct = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))#计算正确

init = tf.global_variables_initializer()
        
sess = tf.Session()
sess.run(init)
visual_a = np.zeros(epoch)
visual_c = np.zeros(epoch)
for k in range(epoch):
    #k=0
    index = [i for i in range(x_tr.shape[0])]
    random.shuffle(index)#打乱索引
    l_r = lrmin+(lrmax-lrmin)*np.exp(-k/epoch)#设置学习速率随时间衰减
    for i in range(int(x_tr.shape[0]/n)):
        #i=0
        mini_x = x_tr[index[i*n:(i+1)*n]].reshape(-1,38,41,1)#以打乱的索引抽取数据组成迷你批
        mini_y = y_tr[index[i*n:(i+1)*n]]
        train_data={x: mini_x, y_: mini_y,lr: l_r}
        
        sess.run(train_step,train_data) 
    c = sess.run(cross_entropy,train_data)    
    a = sess.run(accuracy,test_data) 
    visual_a[k] = a
    visual_c[k] = c
    print("epoch {0}:accuracy:{1},loss:{2}".format(k,a,c))
np.savetxt('a_conv_10.out',visual_a,delimiter=",") 
np.savetxt('c_conv_10.out',visual_c,delimiter=",")    

################################
#残差神经网络
################################
########################
#20层残差神经网络
########################
def res_block(input_layer, filter_shape):
    
    with tf.variable_scope('conv1'):
        conv1 = bn_relu_conv_layer(input_layer,filter_shape)
    with tf.variable_scope('conv2'):   
        conv2 = bn_relu_conv_layer(conv1,filter_shape)
    output = input_layer + conv2 
    
    return output

def inference(n,x):#网络结构
    
    layers = []
    with tf.variable_scope('conv0'):
        filter = tf.get_variable('filter', [3, 3, 1, 10],
                             initializer=tf.truncated_normal_initializer(0,0.1)) 
        conv0 = tf.nn.conv2d(x, filter, [1, 1, 1, 1], padding='SAME')
        layers.append(conv0)
        
    for i in range(int((n-1)/2)):
        with tf.variable_scope('res_block%d' %i):
            res = res_block(layers[-1], [3, 3, 10, 10])
            layers.append(res)
            
    with tf.variable_scope('conv2'):
        conv2 = bn_relu_conv_layer(layers[-1], [3, 2, 10, 10], padding='VALID')
        print(conv2.shape)
        layers.append(conv2)
        
    with tf.variable_scope('fc'):
        bn = bn_layer(layers[-1])
        relu = tf.nn.relu(bn)
        fc_ = tf.nn.max_pool(relu,[1,2,2,1],[1,2,2,1],padding='VALID')
        fc_in = tf.reshape(fc_, shape=[-1, 18*20*10])
        print(fc_.shape)
        fc_w = tf.get_variable('fc_w', [18*20*10,2],
                               initializer=tf.truncated_normal_initializer(0,0.1))
        logits = tf.nn.relu(tf.matmul(fc_in, fc_w))
        
    return logits

x = tf.placeholder(tf.float32, [None, 38,41,1])
y_ = tf.placeholder(tf.float32, [None, 2])
lr = tf.placeholder(tf.float32)

    


n = 15#每个迷你批包含的数据数量
epoch = 100#周期
lrmin = 0.0001#最小学习速率
lrmax = 0.001#最大学习速率

test_data={x: x_te.reshape(-1,38,41,1), y_: y_te}
logits = inference(20,x)
y = tf.nn.softmax(logits)
cross_en = tf.nn.softmax_cross_entropy_with_logits(logits=logits, 
                                                   labels=y_)#损失函数使用交叉熵函数
cross_entropy = tf.reduce_mean(cross_en)
tf.summary.scalar('loss',cross_entropy)
train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)#使用比随机梯度下降效果更好的adam
is_correct = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))#计算正确
tf.summary.scalar('accuracy', accuracy)
init = tf.global_variables_initializer()
        
sess = tf.Session()
sess.run(init)
visual_a = np.zeros(epoch)
visual_c = np.zeros(epoch)
for k in range(epoch):
    #k=0
    index = [i for i in range(x_tr.shape[0])]
    random.shuffle(index)#打乱索引
    l_r = lrmin+(lrmax-lrmin)*np.exp(-k/epoch)#设置学习速率随时间衰减
    for i in range(int(x_tr.shape[0]/n)):
        #i=0
        mini_x = x_tr[index[i*n:(i+1)*n]].reshape(-1,38,41,1)#以打乱的索引抽取数据组成迷你批
        mini_y = y_tr[index[i*n:(i+1)*n]]
        train_data={x: mini_x, y_: mini_y,lr: l_r}
        
        sess.run(train_step,train_data) 
    c = sess.run(cross_entropy,train_data)    
    a = sess.run(accuracy,test_data) 
    visual_a[k] = a
    visual_c[k] = c
    print("epoch {0}:accuracy:{1},loss:{2}".format(k,a,c))
np.savetxt('a_res_20.out',visual_a,delimiter=",") 
np.savetxt('c_res_20.out',visual_c,delimiter=",")    
    
###############################
#神经网络的结果可视化
###############################
epoch = 100
out = {}
names = ['a_conv_d_3.out','c_conv_d_3.out','a_conv_bn_3.out','c_conv_bn_3.out',
         'a_conv_10.out','c_conv_10.out','a_conv_20.out','c_conv_20.out',
         'a_res_20.out','c_res_20.out']
for name in names:
    f = open(name)
    f_read = f.readlines()
    f.close()
    out[name] = np.zeros(epoch)
    i = 0
    for line in f_read:
        out[name][i] = line
        i+=1



y1 = out['a_conv_d_3.out']
y2 = out['a_conv_bn_3.out']
x = range(epoch)
plt.figure()
plt.plot(x,y1,'',label='conv_d_3')
plt.plot(x,y2,'',label='conv_bn_3')
plt.title('accuracy of conv_d and conv_bn')
plt.legend(loc='upper right')
plt.xlabel('epoch')
plt.ylabel('accuracy')
plt.show()

y1 = out['c_conv_d_3.out']
y2 = out['c_conv_bn_3.out']
x = range(epoch)
plt.figure()
plt.plot(x,y1/100.0,'',label='conv_d_3')
plt.plot(x,y2/100.0,'',label='conv_bn_3')
plt.title('loss of conv_d and conv_bn')
plt.legend(loc='upper right')
plt.xlabel('epoch')
plt.ylabel('cross entropy')
plt.show()  

y1 = out['c_conv_bn_3.out']
y2 = out['c_conv_10.out']
y3 = out['c_conv_20.out']
x = range(epoch)
plt.figure()
plt.plot(x,y1/100,'',label='conv_3')
plt.plot(x,y2,'',label='conv_10')
plt.plot(x,y3,'',label='conv_20')
plt.title('loss of conv')
plt.legend(loc='upper right')
plt.xlabel('epoch')
plt.ylabel('cross entropy')
plt.show() 

y1 = out['a_conv_bn_3.out']
y2 = out['a_conv_10.out']
y3 = out['a_conv_20.out']
x = range(epoch)
plt.figure()
plt.plot(x,y1,'',label='conv_3')
plt.plot(x,y2,'',label='conv_10')
plt.plot(x,y3,'',label='conv_20')
plt.title('accuracy of conv')
plt.legend(loc='upper right')
plt.xlabel('epoch')
plt.ylabel('accuracy')
plt.show() 

y1 = out['c_conv_20.out']
y2 = out['c_res_20.out']
x = range(epoch)
plt.figure()
plt.plot(x,y1,'',label='conv_20')
plt.plot(x,y2,'',label='res_20')
plt.title('loss of conv and res')
plt.legend(loc='upper right')
plt.xlabel('epoch')
plt.ylabel('cross entropy')
plt.show()

y1 = out['a_conv_20.out']
y2 = out['a_res_20.out']
x = range(epoch)
plt.figure()
plt.plot(x,y1,'',label='conv_20')
plt.plot(x,y2,'',label='res_20')
plt.title('accuracy of conv and res')
plt.legend(loc='upper right')
plt.xlabel('epoch')
plt.ylabel('accuracy')
plt.show()
######################################收工，撒花~~~~