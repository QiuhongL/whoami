# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 12:28:20 2017

@author: Lai
"""

import numpy as np
import random
import tensorflow as tf
import math

###读入数据
xdigit_f = open('azip.dat')
ydigit_f = open('dzip.dat')
test_y_f = open('dtest.dat')
test_x_f = open('testzip.dat')
xdigit = xdigit_f.readlines()
ydigit = ydigit_f.readlines()
test_y = test_y_f.readlines()
test_x = test_x_f.readlines()
xdigit_f.close()
ydigit_f.close()
test_x_f.close()
test_y_f.close()

###将数据存为数组
x_tr =  np.zeros((1707,256),dtype = np.float32) 
i = 0
for line in xdigit:
    li =line.strip(' ').split('  ')
    x_tr[:,i] = li
    i+=1

y_tr = np.zeros((1707,10),dtype = np.float32)
i = 0
yli = ydigit[0].strip(' ').split('  ')
for yr in yli:
    y_tr[i,int(float(yr))] = 1
    i += 1

x_te =  np.zeros((2007,256),dtype = np.float32)
i = 0
for line in test_x:
    li =line.strip(' ').split('  ')
    x_te[:,i] = li
    i+=1
x_te = x_te.reshape(-1,16,16,1)  
  
y_te = np.zeros((2007,10),dtype = np.float32)
i = 0
yli = test_y[0].strip(' ').split('  ')
for yr in yli:
    y_te[i,int(float(yr))] = 1
    i += 1
    
del i,li,line,test_x,test_y,xdigit,ydigit,yli,yr 

x = tf.placeholder(tf.float32, [None, 16,16,1])
y_ = tf.placeholder(tf.float32, [None, 10])
lr = tf.placeholder(tf.float32)
pkeep = tf.placeholder(tf.float32)
w1 = tf.Variable(tf.truncated_normal([3,3,1,10] ,stddev=0.1))
b1 = tf.Variable(tf.ones([10])/10)
w2 = tf.Variable(tf.truncated_normal([3,3,10,20] ,stddev=0.1))
b2 = tf.Variable(tf.ones([20])/10)
w3 = tf.Variable(tf.truncated_normal([3,3,20,30] ,stddev=0.1))
b3 = tf.Variable(tf.ones([30])/10)
w4 = tf.Variable(tf.truncated_normal([30*256,30] ,stddev=0.1))
b4 = tf.Variable(tf.zeros([30]))
w5 = tf.Variable(tf.truncated_normal([30,10] ,stddev=0.1))
b5 = tf.Variable(tf.zeros([10]))
test_data={x: x_te, y_: y_te}

stride = 1
a1 = tf.nn.relu(tf.nn.conv2d(x, w1, strides=[1, stride, stride, 1], 
                             padding='SAME') + b1)
a1d = tf.nn.dropout(a1,pkeep)
a2 = tf.nn.relu(tf.nn.conv2d(a1, w2, strides=[1, stride, stride, 1], 
                             padding='SAME') + b2)
a2d = tf.nn.dropout(a2,pkeep)
a3 = tf.nn.relu(tf.nn.conv2d(a2, w3, strides=[1, stride, stride, 1], 
                             padding='SAME') + b3)
a3d = tf.nn.dropout(a3,pkeep)
yy = tf.reshape(a3, shape=[-1, 256*30])

a4 = tf.nn.relu(tf.matmul(yy, w4) + b4)
a4d = tf.nn.dropout(a4,pkeep)
ylogits = tf.matmul(a4d, w5) + b5
y = tf.nn.softmax(ylogits)

cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=ylogits, 
                                                        labels=y_)
cross_entropy = tf.reduce_mean(cross_entropy)*100
train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)
is_correct = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

n = 10
epoch = 100
lrmin = 0.0001
lrmax = 0.003
for k in range(epoch):
    #k=0
    index = [i for i in range(x_tr.shape[0])]
    random.shuffle(index)
    l_r = lrmin+(lrmax-lrmin)*math.exp(-k/epoch)
    for i in range(int(x_tr.shape[0]/n)):
        #i=0
        mini_x = x_tr[index[i*n:(i+1)*n]].reshape(-1,16,16,1)
        mini_y = y_tr[index[i*n:(i+1)*n]]
        train_data={x: mini_x, y_: mini_y,lr: l_r,pkeep:0.75}
        test_data={x: x_te, y_: y_te,pkeep:1.0}

        sess.run(train_step,train_data)
    a= sess.run(accuracy,test_data) 
    print("epoch {0}:{1}".format(k,a))
