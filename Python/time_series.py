# -*- coding: utf-8 -*-
"""
Created on Fri Aug 10 18:21:42 2018

@author: Lai
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller,arma_order_select_ic
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima_model import ARIMA,ARMA
from statsmodels.stats import diagnostic
import pickle as pkl
import os   
import arch 
from arch.univariate import StudentsT,GARCH,ConstantMean 
from scipy.stats import shapiro


def test_stationarity(timeseries):
    
    rolmean = pd.Series.rolling(timeseries,window=30,center=False).mean()
    rolstd = pd.Series.rolling(timeseries,window=30,center=False).std()
    
    #plot rolling statistics:
    f = plt.figure()
    f.add_subplot()
    plt.plot(timeseries, color = 'blue',label='Original')
    plt.plot(rolmean , color = 'red',label = 'rolling mean')
    plt.plot(rolstd, color = 'black', label= 'Rolling standard deviation')
    
    plt.legend(loc = 'best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show(block=False)

    #Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    dftest = adfuller(timeseries,autolag = 'AIC')
    #dftest
    dfoutput = pd.Series(dftest[0:4],index = ['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical value (%s)' %key] = value
    
    print(dfoutput) 
        
def draw_acf_pacf(ts, lags=15):
    f = plt.figure()
    ax1 = f.add_subplot(211)
    plot_acf(ts, lags=lags, ax=ax1)
    ax2 = f.add_subplot(212)
    plot_pacf(ts, lags=lags, ax=ax2)
    plt.show()
    
######################## 
work_path = 'root/crude'
with open(work_path+'/wordlist_and_WTI.pkl','rb') as f:
    data = pkl.load(f)
    WTI = data['WTI']  
ts_index = WTI.index[:650]
ts = WTI[:650]
ts.index = range(650)
te_index = WTI.index[650:950]
test_ts =WTI[650:950] 
ts_log = np.log(ts)
ts_log_diff = ts_log.diff(1)
ts_log_diff.dropna(inplace=True)
shapiro(ts_log_diff) # still reject H0 
test_stationarity(ts_log_diff) # 平稳
draw_acf_pacf(ts_log_diff) # arima(1,1,0) arima(1,1,1) arima(0,1,1)
order_ic = arma_order_select_ic(ts_log_diff,max_ar=3,max_ma=3,ic=['aic','bic','hqic'],trend='nc')
order_ic.aic_min_order # (3,2)
order_ic.bic_min_order # (0,1)
order_ic.hqic_min_order # (0,1)
# training
results_ARIMA1 = ARIMA(ts_log, order=(3,1,2)).fit(disp=-1)  
results_ARIMA1.summary() # 
results_ARIMA2 = ARIMA(ts_log, order=(1,1,0)).fit(disp=-1)  
results_ARIMA2.summary()
results_ARIMA3 = ARIMA(ts_log, order=(0,1,1)).fit(disp=-1)  
results_ARIMA3.summary()
os.mkdir(work_path+'/ts_model')
order_list = [(1,1,0),(0,1,1)]
for i,order in enumerate(order_list):
    results_ARIMA = eval('results_ARIMA{}'.format(str(i+1)))
    f = plt.figure() 
    f.add_subplot(111)
    plt.plot(ts_log_diff)
    plt.plot(results_ARIMA.fittedvalues, color='red')
    plt.title('RSS: %.4f'% sum((results_ARIMA1.fittedvalues-ts_log_diff)**2))
    plt.show() # RSS 0.4867 0.4867
    predict_ts = results_ARIMA.fittedvalues
    diff_shift_ts = ts_log.shift(1)
    predictions = predict_ts.add(diff_shift_ts)
    predictions_ARIMA = np.exp(predictions)
    predictions_ARIMA.index = ts_index
    ts.index = ts_index
    f = plt.figure()
    plt.plot(ts)
    plt.plot(predictions_ARIMA)
    plt.title('ARIMA RMSE: %.4f' % np.sqrt(np.sum((predictions_ARIMA - ts) ** 2) / len(ts)))
    plt.show()
    f.savefig(work_path+'/ts_model/arima{}.png'.format(str(order)))
    results_ARIMA.params

# rolling forcast
ts_log_all = np.log(WTI[:950])
ts_log_all.index = range(950)
ts_log_pred = pd.Series([0.0]*300)
ts_log_pred.index = range(650,950)
for order in order_list:
    for i in range(650,950):
        ts_log = ts_log_all[:i]
        ts_log_diff = ts_log.diff(1)
        ts_log_diff.dropna(inplace=True)
        model_results = ARIMA(ts_log, order).fit(disp=-1) 
        ts_log_pred[i] = model_results.forecast(1)[0]
    ts_pred = np.exp(ts_log_pred)  
    ts_pred.index = WTI.index[650:950]  
    f = plt.figure()
    f.add_subplot(111)
    plt.plot(ts_pred)   
    plt.plot(test_ts)
    plt.title('forecast_ARIMA{0} RMSE: {1}'.format(str(order),np.sqrt(np.sum((ts_pred - test_ts) ** 2) / len(test_ts))))
    plt.show()
    f.savefig(work_path+'/ts_model/arima{}_forecast.png'.format(str(order)))
 
 
# garch 
*_,pvalue = diagnostic.het_arch(results_ARIMA2.resid)
pvalue # reject H0
f = plt.figure()
f.add_subplot(111) 
res2 = results_ARIMA2.resid**2
plot_pacf(res2,lags=50) 
plot_acf(res2,lags=50)
#garch_model = arch.arch_model(ts_log_diff*10,mean='AR',lags=1,vol='GARCH',p=1)
#garch_model.distribution=StudentsT()
#garch = garch_model.fit()
#garch.summary() # nonsignificant when p=1-3 thus try ConstantMean model
# constantmean-garch model
garch_model = ConstantMean(ts_log_diff*10000)
garch_model.volatility = GARCH(1,0,1)
garch_model.distribution = StudentsT()
garch1 = garch_model.fit()
garch1.summary() 
garch1.hedgehog_plot()
garch_model.volatility = GARCH(1,0,0)
garch2 = garch_model.fit()
garch2.summary() 
garch2.hedgehog_plot()
# arma-garch model
res = results_ARIMA2.resid
garch_model = ConstantMean(res*10000)
garch_model.volatility = GARCH(1,0,1)
garch_model.distribution = StudentsT()
garch3 = garch_model.fit()
garch3.summary() 
garch_model = ConstantMean(res*10000)
garch_model.volatility = GARCH(1,0,0)
garch_model.distribution = StudentsT()
garch4 = garch_model.fit()
garch4.summary() 
res = results_ARIMA3.resid
garch_model = ConstantMean(res*10000)
garch_model.volatility = GARCH(1,0,1)
garch_model.distribution = StudentsT()
garch5 = garch_model.fit()
garch5.summary() 
garch_model = ConstantMean(res*10000)
garch_model.volatility = GARCH(1,0,0)
garch_model.distribution = StudentsT()
garch6 = garch_model.fit()
garch6.summary() 

### rolling forecast
# rt = mu + sigmat*zt sigma**2 = omega + alpha*rt-1**2 + beta*sigma**2
ts_log_all = np.log(WTI[:950])
ts_log_all.index = range(950)
ts_log_diff_pred = pd.Series([0.0]*300)
ts_log_diff_pred.index = range(650,950)
order_list = [(1,0,1),(1,0,0)]
for order in order_list:
    for i in range(650,950):
        ts_log = ts_log_all[(i-650):i]
        ts_log_diff = ts_log.diff(1)
        ts_log_diff.dropna(inplace=True)
        garch_model = ConstantMean(ts_log_diff*10000)
        garch_model.volatility = eval('GARCH'+str(order))
        garch_model.distribution = StudentsT()
        garch = garch_model.fit()
        if order[2]==1:
            _,omega,alpha,beta,_ = garch.params
            sigmat = 0.01*np.sqrt(omega+alpha*garch.resid.iloc[-1]**2+beta*garch.conditional_volatility.iloc[-1]**2)
        else:
            _,omega,alpha,_ = garch.params
            sigmat = 0.01*np.sqrt(omega+alpha*garch.resid.iloc[-1]**2)
        rt = sigmat*np.random.standard_normal()
        ts_log_diff_pred[i] = rt/10000
    ts_pred = np.exp(ts_log_diff_pred.add(ts_log_all[649:950].shift(1).dropna()))    
    ts_pred.index = WTI.index[650:950]  
    f = plt.figure()
    f.add_subplot(111)
    plt.plot(ts_pred)   
    plt.plot(test_ts)
    p,_,q = order
    plt.title('forecast_garch({0},{1}) RMSE: {2}'.format(str(p),str(q),np.sqrt(np.sum((ts_pred - test_ts) ** 2) / len(test_ts))))
    f.savefig(work_path+'/ts_model/garch({0},{1})_forecast.png'.format(str(p),str(q)))
    plt.show()
###    
order_list = [(1,0,1),(1,0,0)] 
arima_list = [(1,0),(0,1)]
for a in arima_list:   
    for order in order_list:
        for i in range(650,950):
            ts_log = ts_log_all[:i]
            ts_log_diff = ts_log.diff(1)
            ts_log_diff.dropna(inplace=True)
            model_results = ARMA(ts_log_diff, a).fit(disp=-1) 
            mu_arma = model_results.forecast(1)[0]
            res = model_results.resid
            garch_model = ConstantMean(res*10000)
            garch_model.volatility = eval('GARCH'+str(order))
            garch_model.distribution = StudentsT()
            garch = garch_model.fit()
            if order[2]==1:
                _,omega,alpha,beta,_ = garch.params
                sigmat = 0.01*np.sqrt(omega+alpha*garch.resid.iloc[-1]**2+beta*garch.conditional_volatility.iloc[-1]**2)
            else:
                _,omega,alpha,_ = garch.params
                sigmat = 0.01*np.sqrt(omega+alpha*garch.resid.iloc[-1]**2)
            rt = sigmat*np.random.standard_normal()
            ts_log_diff_pred[i] = mu_arma+rt/10000
        ts_pred = np.exp(ts_log_diff_pred.add(ts_log_all[649:950].shift(1).dropna())) 
        ts_pred.index = WTI.index[650:950]  
        f = plt.figure()
        f.add_subplot(111)
        plt.plot(ts_pred)   
        plt.plot(test_ts)
        a1,a2 = a
        p,_,q = order
        plt.title('forecast_arima({0},1,{1})-garch({2},{3}) RMSE: {4}'.format(str(a1),str(a2),str(p),str(q),np.sqrt(np.sum((ts_pred - test_ts) ** 2) / len(test_ts))))
        f.savefig(work_path+'/ts_model/arima({0},1,{1})garch({2},{3})_forecast.png'.format(str(a1),str(a2),str(p),str(q)))
        plt.show()

