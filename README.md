# Who Am I
---
This public repository shows some code copied from my private repositories. 
There are three parts of the repository:

* **Python**

>This directory contains some Python code I wrote during 2017 to 2018 involving deep learning and machine learning.

* **R**

>There is some R code I wrote during 2017 to 2018 involving machine learning, algebra, statistic and data analysis.

* **FormulaDerivation(TBC)**

>Some interesting formula derivation I have done was shown in this directory.

Hope this repository 'Who Am I' could help you get to know me better.